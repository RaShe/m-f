import utilsService from '../services/utils.service';
import fetchService from '../services/fetch.service';

class CasesStore {
    casesObjListBySlug = {};
    allDataLoaded = false;

    async getCasesList() {
        if (!this.allDataLoaded) {
            const response = await fetchService.get(
                `https://run.mocky.io/v3/553b178a-f41f-4062-8a3a-38e79881b308`);
            this.casesObjListBySlug = utilsService.normalizeApiData(response);
        }
    }
}

const casesStore = new CasesStore();

export default casesStore;
