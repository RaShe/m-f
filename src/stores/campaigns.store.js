import { action, observable } from 'mobx';

import fetchService from '../services/fetch.service';

class CampaignsStore {
    @observable campaignsListById = {};

    async getCampaignDataById(campaignId, offset = 0) {
        if (!this.campaignsListById[campaignId] || offset) {
            const response = await fetchService.authenticatedGet(
                `https://dev.withminta.com/generate-video/videos/findByCampaign?campaignId=${campaignId}&offset=${offset}&limit=6&applicationSource=web`);
            this.mergeCampaigns(campaignId, response);
        }
    }

    @action.bound
    mergeCampaigns(campaignId, response) {
        if (!this.campaignsListById[campaignId]) {
            this.campaignsListById[campaignId] = response;
        } else {
            response.docs = [...this.campaignsListById[campaignId].docs, ...response.docs];
            this.campaignsListById[campaignId] = response;
        }
    }
}

const campaignsStore = new CampaignsStore();

export default campaignsStore;
