import React, { PureComponent } from 'react';
import LeftSidebarComponent from '../components/leftSidebar/LeftSidebar.component';
import CampaignComponent from '../components/campaign/Campaign.component';
import casesStore from '../../stores/cases.store';

class MintsView extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            sideBarLoader: true
        };
    }

    async componentDidMount() {
        const { slug } = this.props.match.params;

        if (!casesStore.allDataLoaded) {
            await casesStore.getCasesList();
        }

        const caseList = Object.values(casesStore.casesObjListBySlug);

        if (!slug) {
            this.props.history.push(`/mints/${caseList[0].slug}`);
        } else {
            this.setState({
                sideBarLoader: false
            });
        }
    }

    getCampaignId(slug) {
        let result = null;
        const caseItem = casesStore.casesObjListBySlug[slug];
        if (caseItem && caseItem.campaignId) {
            result = caseItem.campaignId;
        }
        return result;
    }

    render() {
        const { slug } = this.props.match.params;
        const { sideBarLoader } = this.state;

        const casesList = Object.values(casesStore.casesObjListBySlug);
        const isCaseList = casesList && casesList.length > 0;

        return (
            <section className="with-sidebar mints">
                <LeftSidebarComponent
                    caseList={casesList}
                    loader={sideBarLoader}
                />
                <main>
                    {isCaseList && (
                        <CampaignComponent
                            campaignId={this.getCampaignId(slug)}
                            caseSlug={slug}
                        />
                    )}
                </main>
            </section>
        );
    }
}

export default MintsView;
