import React, { Component } from 'react';
import { observer } from 'mobx-react';
import campaignsStore from '../../../stores/campaigns.store';
import Loader from '../loader/Loader.component';
import VideoListComponent from './components/video/VideoList.component';
import HeaderComponent from './components/header/Header.component';
import casesStore from '../../../stores/cases.store';
import './campaign.scss';

@observer
class CampaignComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: true,
            showMoreButtonLoading: false
        };
    }

    async componentDidMount() {
        const { campaignId } = this.props;
        await this.getCampaignData(campaignId);
    }

    async componentDidUpdate(prevProps, prevState, snapshot) {
        const { campaignId } = this.props;
        if (prevProps.campaignId !== campaignId) {
            this.setState({
                loader: true
            });
            await this.getCampaignData(campaignId);
        }
    }

    getCampaignData = async campaignId => {
        await campaignsStore.getCampaignDataById(campaignId);
        this.setState({
            loader: false
        });
    };

    showMoreHandle = async e => {
        const { showMoreButtonLoading } = this.state;

        const { campaignId } = this.props;
        const campaignData = campaignsStore.campaignsListById[campaignId];

        e.preventDefault();

        if (!showMoreButtonLoading) {
            this.setState({
                showMoreButtonLoading: true
            });
            await campaignsStore.getCampaignDataById(campaignId, campaignData.offset + 1);
            this.setState({
                showMoreButtonLoading: false
            });
        }
    };

    getVideos(campaignData) {
        let videos = [];
        if (campaignData && campaignData.docs && campaignData.docs.length > 0) {
            campaignData.docs.forEach(doc => {
                if (doc.videos && doc.videos.length > 0) {
                    videos.push(doc.videos[0]);
                }
            });
        }
        return videos;
    }

    getTitle(caseSlug) {
        let result = 'No name';
        const caseData = casesStore.casesObjListBySlug[caseSlug];
        if (caseData) {
            result = caseData.name;
        }

        return result;
    }

    render() {
        const { campaignId, caseSlug } = this.props;
        const { loader, showMoreButtonLoading } = this.state;
        const campaignData = campaignsStore.campaignsListById[campaignId];

        const videos = this.getVideos(campaignData);
        const isMoreAvailable = campaignData && campaignData.hasNextPage;

        return (
            <div className='campaign'>
                {loader && (
                    <Loader/>
                )}

                {!loader && (
                    <>
                        <HeaderComponent
                            data={this.getTitle(caseSlug)}
                        />
                        <VideoListComponent
                            data={videos}
                        />

                        {isMoreAvailable && (
                            <div className="show-more-wrapper">
                                <button
                                    onClick={this.showMoreHandle}
                                    className={showMoreButtonLoading ? 'button loading' : 'button'}>
                                    Show More
                                </button>
                            </div>
                        )}
                    </>
                )}
            </div>
        );
    }
}

export default CampaignComponent;
