import React from 'react';
import VideoItemComponent from './VideoItem.component';
import './videoList.scss';

const VideoListComponent = props => {
    const { data } = props;

    return (
        <div className='video-list'>
            {data.map((video, idx) => {
                return (
                    <VideoItemComponent
                        key={idx + 'video-item'}
                        data={video}
                    />
                );
            })}
        </div>
    );
};

export default VideoListComponent;
