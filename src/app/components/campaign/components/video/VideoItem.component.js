import React, { PureComponent } from 'react';
import './videoItem.scss';

class VideoItemComponent extends PureComponent {
    videoPlayHandle = () => {
        this.refs.videoRef.play();
    };

    videoStopHandle = () => {
        this.refs.videoRef.pause();
    };

    render() {
        let { data } = this.props;
        const videoUrl = data.url;
        const videoPreviewImage = data.previewImage;

        return (
            <div
                onMouseEnter={this.videoPlayHandle}
                onMouseLeave={this.videoStopHandle}
                className='video-item'>
                <div className="stale-layer">
                    <div className="stale-layer-play-icon">
                        ▶
                    </div>
                </div>
                <video
                    ref='videoRef'
                    width={226} height={282} src={videoUrl} poster={videoPreviewImage}/>
            </div>
        );
    }
}

export default VideoItemComponent;
