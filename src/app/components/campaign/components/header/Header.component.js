import React from 'react';
import './header.scss';

const HeaderComponent = ({ data }) => {
    return (
        <header className='campaign-header'>
            <h1 className='title'>
                {`# ${data}`}
            </h1>
        </header>
    );
};

export default React.memo(HeaderComponent);
