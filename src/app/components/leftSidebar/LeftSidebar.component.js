import React from 'react';
import './leftSidebar.scss';
import { NavLink } from 'react-router-dom';
import Loader from '../loader/Loader.component';

const LeftSidebarComponent = props => {
    const { caseList, loader } = props;

    return (
        <aside className='left-sidebar'>
            <div className="sidebar-wrapper">
                <h2 className='title'>Mints</h2>
                {loader && (
                    <Loader/>
                )}

                {!loader && (
                    <ul>
                        {caseList.map(item => {
                            if (item.isEnabled) {
                                return (
                                    <li
                                        title={item.description}
                                        key={item._id}>
                                        <NavLink
                                            className='left-sidebar-link'
                                            activeClassName='active'
                                            to={`/mints/${item.slug}`}>
                                            {`# ${item.name}`}
                                        </NavLink>
                                    </li>
                                );
                            } else {
                                return null;
                            }
                        })}
                    </ul>
                )}
            </div>
        </aside>
    );
};

export default LeftSidebarComponent;
