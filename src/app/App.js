import React, { lazy, Suspense } from 'react';

import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';

import '../styles/app.scss';
import Loader from './components/loader/Loader.component';

const MintsView = (lazy(() => (import('./pages/Mints.view'))));

const App = () => {
    return (
        <section className='page'>
            <Router>
                <Suspense fallback={<Loader/>}>
                    <Switch>
                        <Redirect from="/" exact to="/mints"/>
                        <Route path='/mints/:slug'
                               component={props => <MintsView {...props}/>}/>
                        <Route path='/mints'
                               component={props => <MintsView {...props}/>}/>
                    </Switch>
                </Suspense>
            </Router>
        </section>
    );
};

export default App;
