class FetchService {
    get = url => {
        const headers = {
            'Content-Type': 'application/json'
        };
        const request = new Request(url, {
            method: 'GET',
            headers: headers
        });

        return fetch(request).then((response) => {
                if (response) {
                    return (this.checkResponse(response));
                }
            }
        );
    };

    authenticatedGet = async (url) => {
        //TODO: In real app we should store it in mobx
        const headers = { 'Authorization': `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTg0OGQ2YWU1MWMwNzQ5ODRhYTdlYjEiLCJyb2xlcyI6WyJ1c2VyIl0sImlhdCI6MTU4NTc0NTI1OSwiZXhwIjoxNTg1ODMxNjU5fQ.S61K8RkHJ6qwxRjp9m2Pfvttd6hRBOyWRO3TimRkJA4` };
        const request = new Request(url, {
            method: 'GET',
            headers: headers
        });

        return fetch(request).then((response) => {
                if (response) {
                    return (this.checkResponse(response));
                }
            }
        );

    };

    checkResponse = response => {
        const status = response.status;
        if (status === 200) {
            return (response.json());
        }
    };
}

const fetchService = new FetchService();
export default fetchService;
