import React from 'react';

class UtilsService {
    doMeAFavor(cont) {
        return JSON.parse(JSON.stringify(cont));
    }

    normalizeApiData = (data, id = 'slug') => {
        const tempData = {};
        if (data) {
            for (let i = 0; i < data.length; i++) {
                tempData[data[i][id]] = data[i];
            }
        }
        return tempData;
    };

    mergeApiData = (dataFromApi, localData) => {
        return Object.assign(localData, dataFromApi);
    };
}

const utilsService = new UtilsService();
export default utilsService;
